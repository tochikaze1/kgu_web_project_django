from django.shortcuts import render
from blog.models import Article, PlantsForSale, City
from django.http import Http404, HttpResponseRedirect, HttpResponse
import requests

def main(request):
    list_plantsforsale = PlantsForSale.objects.order_by('-date')
    return render(request, './main.html', {'list_plantsforsale': list_plantsforsale } ) 

def payment(request):
    return render(request, 'blog/payment.html', {})

def login(request):
    return render(request, 'account/login.html', {})

def logout(request):
    return render(request, 'account/logout.html', {})
from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'blog'

urlpatterns = [
    path('', views.main, name='main'),
    #path('<int:article_id>', views.single, name='single'),
    #path('<int:plantsforsale_id>', views.firstaricle, name='firstaricle'),
    path('payment', views.payment, name='payment'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
] 
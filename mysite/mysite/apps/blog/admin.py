from django.contrib import admin
from blog.models import Article, Comment, PlantsForSale, InfoArticle

admin.site.register(PlantsForSale)
admin.site.register(InfoArticle)